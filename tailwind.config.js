/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    container: {
      padding: '15px'
    },
    extend: {
      colors: {
        primary: '#EF3340',
        'primary-darken': '#870F00'
      },
      spacing: {
        '9': '2.25rem',
        '23': '5.75rem',
        '25': '6.25rem',
        '80': '20rem',
        '96': '24rem'
      },
      borderRadius: {
        xl: '1rem',
        '2xl': '1.5rem'
      },
      boxShadow: {
        '4xl': '0 40px 120px rgba(0, 0, 0, 0.1)'
      }
    }
  },
  variants: {
    padding: ['responsive', 'last', 'odd', 'even'],
    margin: ['responsive', 'first', 'last'],
    border: ['first', 'last'],
    borderRadius: ['responsive', 'first', 'last'],
    justifyContent: ['responsive', 'odd']
  },
  plugins: []
};
